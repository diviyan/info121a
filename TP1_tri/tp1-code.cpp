#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <cmath>

using namespace std;

struct Ville {
	string nom;
	string code;
	float  longitude;
	float  latitude;
	int    population;
	double distParis;
};
vector<Ville> liste;

void permute(int i, int j) {
	Ville temp = liste[i];
	liste[i] = liste[j];
	liste[j] = temp;
}
Ville trouve_Paris(){
	for (size_t i = 0; i < liste.size(); i++){
		if (liste[i].nom=="Paris"){
			return liste[i];
		}
	}
}

void calcule_distParis(){
	Ville Paris = trouve_Paris();
	for (size_t i = 0; i < liste.size(); i++){
		liste[i].distParis = sqrt(pow((liste[i].longitude - Paris.longitude) *
																	cos((liste[i].latitude+Paris.latitude)/2), 2)
														  + pow(liste[i].latitude - Paris.latitude, 2));
	}
}

bool compare(int i, int j) {
if (liste[i].population > liste[j].population){
	return true;
}else{
	return false;
}
}

void triselection (int n) {
	for (int k = 0; k < n - 1; k++) {
		int meilleur = k;
		for (int i = k + 1; i < n; i++)
			if (liste[meilleur].population > liste[i].population)
				meilleur = i;
		permute(k, meilleur);
	}
}

void triinsertion_permute (int n) {
	/* Tri à insertion qui utilise la fonction permute
	   et fait une double boucle: Non optimal !
	*/
	for (int k = 1; k < n; k++) {
		for (int i=k; i>0;i--){
			if (compare(i-1,i)){
				permute(i-1, i);
			}
		}
	}

}
void triinsertion (int n) {
	/* Tri à insertion optimal
	*/
	int i;
	for (int k = 1; k < n; k++) { // on trie les éléments du tableau de gauche à droite.
		bool trie = false;
		Ville elt=liste[k];
		i=k;
		do {
			if (liste[i-1].population > elt.population){
				liste[i]=liste[i-1]; // on décale un élément
				i--;
			}else{
				trie=true;
			}
		} while(not trie && i>0);
		liste[i]=elt;
	}
}

void triabulle (int n){
	for (int k=n-1; k > 0; k--) {
		for (int i=0; i<k; i++){
			if (compare(i, i+1)){
				permute(i+1, i);
			}
		}
	}
}

void triabulle_optimise (int n){
	int i;
	for (int k=n-1; k > 0; k--) {
		i=0;
		bool trie=true;
		for (int i=0; i<k; i++){
			if (compare(i, i+1)){
				permute(i+1, i);
				trie=false;
			}
		}
		if (trie)
			return;
	}

}
void tamisage(int noeud, int n){
	int k = noeud;
	int j = 2*k;
	while(j<=n){
		if (j<n && compare(j+1,j))
			j++;
		if (compare(j, k)){
			permute(j, k);
			k=j;
			j=2*k;
		}else
			j=n+1;
	}

}

void tripartas(int n){
	for (int i=n/2; i>=0; i--){
		tamisage(i, n);
	}
	for (int i=n-1; i>=1; i--){
		permute(i, 0);
		tamisage(0, i-1);
	}
}

double moyenne (){
	double sum=0.0;
	for (size_t i = 0; i < liste.size(); i++)
		sum = sum+liste[i].population;
	return sum/liste.size();
}

double mediane(){
	// necessite une liste au préalable triée.
	double med=0.0;
	if (liste.size() % 2 == 1)
 		med = liste[liste.size() / 2].population;
 	else
   	med = (liste[liste.size() / 2].population
           + liste[liste.size() / 2 + 1].population) / 2;
	return med;
}

void lire_fichier(string s){
	ifstream file(s.c_str());
	string line;
	getline(file, line);
	while (getline(file, line)) {
		Ville v;
		int pos;

		pos          = line.find(";");
		v.nom        = line.substr(0, pos);
		line         = line.substr(pos + 1, line.size() - 1);

		pos          = line.find(";");
		v.code       = line.substr(0, pos);
		line         = line.substr(pos + 1, line.size() - 1);

		pos          = line.find(";");
		v.longitude  = atof(line.substr(0, pos).c_str());
		line         = line.substr(pos + 1, line.size() - 1);

		pos          = line.find(";");
		v.latitude   = atof(line.substr(0, pos).c_str());
		line         = line.substr(pos + 1, line.size() - 1);

		pos          = line.find(";");
		v.population = atoi(line.substr(0, pos).c_str());

		liste.push_back(v);
	}
	file.close();
}

int main(int, char*[]) {
	lire_fichier("tp1-data.csv");
	//triinsertion(liste.size());
	calcule_distParis();
	tripartas(liste.size());
	for (size_t i = 0; i < liste.size(); i++)
		cout << i << " " << liste[i].nom << " " << liste[i].distParis << endl;

	cout << "Moyenne: " << moyenne() << "  Mediane: " << mediane() << endl;

	return 0;
}
