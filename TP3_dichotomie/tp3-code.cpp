#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>

using namespace std;

struct Ville {
	string nom;
	string code;
	float  longitude;
	float  latitude;
	int    population;
};
vector<Ville> liste;

void permute(int i, int j) {
	Ville temp = liste[i];
	liste[i] = liste[j];
	liste[j] = temp;
}

void triselection (int n) {
	for (int k = 0; k < n - 1; k++) {
		int meilleur = k;
		for (int i = k + 1; i < n; i++)
			if (liste[meilleur].population > liste[i].population)
				meilleur = i;
		permute(k, meilleur);
	}
}

int partitionner(int premier, int dernier, int pivot){
  permute(pivot, dernier);
  int j=premier;
  for (int i=premier; i < dernier; i++){
    // on cherche à décaler tous les chiffres jusqu'à trouver la bonne place pour le pivot.
    if (liste[i].population <= liste[dernier].population){
      permute(i, j);
      j++;
    }
  }
  permute(dernier, j);
  return j;
}

void tri_rapide(int premier, int dernier){
  if (premier < dernier){
    int pivot = (dernier + premier)/2;
    pivot = partitionner(premier, dernier, pivot);
    tri_rapide(premier, pivot-1);
    tri_rapide(pivot+1, dernier);
  }

}


void lire_fichier(string s){
	ifstream file(s.c_str());
	string line;
	getline(file, line);
	while (getline(file, line)) {
		Ville v;
		int pos;

		pos          = line.find(";");
		v.nom        = line.substr(0, pos);
		line         = line.substr(pos + 1, line.size() - 1);

		pos          = line.find(";");
		v.code       = line.substr(0, pos);
		line         = line.substr(pos + 1, line.size() - 1);

		pos          = line.find(";");
		v.longitude  = atof(line.substr(0, pos).c_str());
		line         = line.substr(pos + 1, line.size() - 1);

		pos          = line.find(";");
		v.latitude   = atof(line.substr(0, pos).c_str());
		line         = line.substr(pos + 1, line.size() - 1);

		pos          = line.find(";");
		v.population = atoi(line.substr(0, pos).c_str());

		liste.push_back(v);
	}
	file.close();
}

int main(int, char*[]) {
	lire_fichier("tp1-data.csv");
	tri_rapide(0, liste.size()-1);
        // tri_rapide(0, liste.size());

	// tri_rapide(0, liste.size());

	for (size_t i = 0; i < liste.size(); i++)
		cout << i << " " << liste[i].nom << " " << liste[i].population  << endl;

	return 0;
}
