#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>

using namespace std;

struct Ville {
	string *nom;
	string *code;
	float   longitude;
	float   latitude;
	int     population;
	int     departement;
};
vector<Ville *> liste;

struct Departement {
	string *nom;
	int     numero;
	int     population;
	struct Departement *next;
};
Departement *first = NULL;

void lire_fichier(string s){
	ifstream file(s.c_str());
	string line;
	getline(file, line);
	while (getline(file, line)) {
		Ville *v = (Ville *)malloc(sizeof(Ville));
		int pos;

		pos            = line.find(";");
		v->nom         = new string(line.substr(0, pos));
		line           = line.substr(pos + 1, line.size() - 1);

		pos            = line.find(";");
		v->code        = new string(line.substr(0, pos));
		line           = line.substr(pos + 1, line.size() - 1);

		pos            = line.find(";");
		v->longitude   = atof(line.substr(0, pos).c_str());
		line           = line.substr(pos + 1, line.size() - 1);

		pos            = line.find(";");
		v->latitude    = atof(line.substr(0, pos).c_str());
		line           = line.substr(pos + 1, line.size() - 1);

		pos            = line.find(";");
		v->population  = atoi(line.substr(0, pos).c_str());

		v->departement = atoi(v->code->substr(0, 2).c_str());

		struct Departement *ptr = first;
		while (ptr != NULL && ptr->numero != v->departement)
			ptr = ptr->next;
		if (ptr == NULL) {
			ptr = (Departement *)malloc(sizeof(Departement));
			ptr->population = 0;
			ptr->numero     = v->departement;
			ptr->next       = first;
			first = ptr;
		}
		ptr->population = ptr->population + v->population;

		liste.push_back(v);
	}
	file.close();
}

int main(int, char*[]) {
	lire_fichier("tp1-data.csv");

	for (size_t i = 0; i < liste.size(); i++)
		cout << i << " " << *liste[i]->nom
		          << " " << *liste[i]->code << endl;

	struct Departement *ptr = first;
	while (ptr != NULL){
		cout << " le departement No " << ptr->numero
		     << " a une population de " << ptr->population
		     << " habitants" << endl;
		ptr = ptr->next;
	}

	return 0;
}
